# Задание 1. Создать две переменные first=10, second=30. Вывести на экран результат математического взаимодействия (+, -, *, / и тд.) для этих чисел.

first = 10
second = 30

res = first + second
print(res)

res = first - second
print(res)

res = first * second
print(res)

res = first / second
print(res)

res = first // second
print(res)

res = first % second
print(res)

res = first ** second
print(res)

# Задание 2. Создать переменную и поочередно записать в нее результат сравнения (<, > , ==, !=) чисел из задания 1. После этого вывести на экран значение каждой полученной переменной.

numberOne = 12
numberTwo = 25

clarification = numberOne > numberTwo
print(clarification)

clarification = numberOne < numberTwo
print(clarification)

clarification = numberOne >= numberTwo
print(clarification)

clarification = numberOne <= numberTwo
print(clarification)

clarification = numberOne == numberTwo
print(clarification)

clarification = numberOne != numberTwo
print(clarification)

# Задача 3. Создать переменную - результат конкатенации (сложения) строк str1="Hello " и str2="world". Вывести на ее экран.

str1 = 'Hello'
str2 = ' world!'
print(str1 + str2)